package com.example.onlinequiz.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [QuizModel::class], version = 1, exportSchema = false)
@TypeConverters(TimestampConverter::class)
abstract class QuizDatabase : RoomDatabase() {

    abstract fun daoAccess(): DaoAccess?
}