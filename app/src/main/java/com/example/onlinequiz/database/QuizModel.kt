package com.example.onlinequiz.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import java.io.Serializable
import java.util.*

@Entity
data class QuizModel(@ColumnInfo(name = "created_at")  var title: String,var questionModel: String,var createdAt: Date)
{
    @PrimaryKey(autoGenerate = true) var id: Int = 0
}