package com.example.onlinequiz.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class QuizRepo {

    private String DB_NAME = "school_quiz";

    private QuizDatabase noteDatabase;
    public QuizRepo(Context context) {
        noteDatabase = Room.databaseBuilder(context, QuizDatabase.class, DB_NAME).build();
    }

    public void insertTask(String title, String questionModels) {

        insertTask(title,questionModels,null);
    }

    public void insertTask(String title,String questionModels,String wsf) {

        QuizModel quizModel = new QuizModel(title,questionModels,getCurrentDateTime());
        insertTask(quizModel);
    }

    public void insertTask(final QuizModel note) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().insertTask(note);
                return null;
            }
        }.execute();
    }

    public void updateTask(final QuizModel note) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().updateTask(note);
                return null;
            }
        }.execute();
    }

    public void deleteTask(final int id) {
        final LiveData<QuizModel> task = getTask(id);
        if(task != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    noteDatabase.daoAccess().deleteTask(task.getValue());
                    return null;
                }
            }.execute();
        }
    }

    public void deleteTask(final QuizModel note) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().deleteTask(note);
                return null;
            }
        }.execute();
    }

    public LiveData<QuizModel> getTask(int id) {
        return noteDatabase.daoAccess().getTask(id);
    }

    public LiveData<List<QuizModel>> getTasks() {
        return noteDatabase.daoAccess().fetchAllTasks();
    }
    public static Date getCurrentDateTime(){
        Date currentDate =  Calendar.getInstance().getTime();
        return currentDate;
    }

}
