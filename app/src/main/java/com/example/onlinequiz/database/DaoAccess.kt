package com.example.onlinequiz.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface DaoAccess {

    @Insert
    fun insertTask(note: QuizModel?): Long?


    @Query("SELECT * FROM QuizModel ORDER BY created_at desc")
    fun fetchAllTasks(): LiveData<List<QuizModel?>?>?

    @Query("SELECT * FROM QuizModel WHERE id =:taskId")
    fun getTask(taskId: Int): LiveData<QuizModel?>?

    @Update
    fun updateTask(note: QuizModel?)


    @Delete
    fun deleteTask(note: QuizModel?)
}