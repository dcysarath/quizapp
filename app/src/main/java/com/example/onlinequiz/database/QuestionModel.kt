package com.example.onlinequiz.database

import java.io.Serializable

class QuestionModel : Serializable {

    var question: String = ""
    var optionA: String = ""
    var optionB: String = ""
    var optionC: String = ""
    var optionD: String = ""
    var answer: String = ""

}