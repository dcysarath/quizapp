package com.example.onlinequiz.database

import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.room.Room
import java.util.*

class QuizRepository(val context: Context) {

    val DB_NAME = "school_quiz"

    val noteDatabase: QuizDatabase = Room.databaseBuilder(context, QuizDatabase::class.java, DB_NAME).build()

    fun insertTask(title: String,questionModels: String) {
        insertTask(title, questionModels,"")
    }

    fun insertTask(title: String, questionModels: String, wsf: String) {
        val quizModel = QuizModel(title,questionModels,getCurrentDateTime())
        insertTask(quizModel)
    }

    fun insertTask(quizModel: QuizModel?) {
        object : AsyncTask<Void?, Void?, Void?>() {
            override fun doInBackground(vararg p0: Void?): Void? {
                noteDatabase!!.daoAccess()!!.insertTask(quizModel)
                return null
            }
        }.execute()
    }

    fun getCurrentDateTime(): Date {
        return Calendar.getInstance().time
    }

    fun getTask(id: Int): LiveData<QuizModel?>? {
        return noteDatabase!!.daoAccess()!!.getTask(id)
    }

    fun getTasks(): LiveData<List<QuizModel?>?>? {
        return noteDatabase!!.daoAccess()!!.fetchAllTasks()
    }
}