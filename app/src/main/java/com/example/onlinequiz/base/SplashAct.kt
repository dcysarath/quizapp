package com.example.onlinequiz.base

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.example.onlinequiz.R
import com.example.onlinequiz.auth.WelcomeAct
import com.example.onlinequiz.common.IntentKeys
import com.example.onlinequiz.home.QuizListAct

class SplashAct : BaseActivity() {

    //Variable declarations
    lateinit var sharedPreference: SharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_act)

        //Initialize
        sharedPreference = SharedPreference(this)

        //Hide action&status Bar
        hideActionBar()
        transparentToolbar()

        //Handler method
        Handler().postDelayed({
            if(sharedPreference.loginCheck()){
                startActivity(Intent(this, QuizListAct::class.java)
                    .putExtra(IntentKeys.INSTANCE.AUTH_TYPE,sharedPreference.getValueString(IntentKeys.INSTANCE.AUTH_TYPE)))
                finish()
            }else{
                startActivity(Intent(this, WelcomeAct::class.java))
                finish()
            }
        }, 2500)
    }

}