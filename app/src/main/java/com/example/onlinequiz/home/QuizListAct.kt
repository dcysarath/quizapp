package com.example.onlinequiz.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.onlinequiz.R
import com.example.onlinequiz.base.BaseActivity
import com.example.onlinequiz.base.SharedPreference
import com.example.onlinequiz.common.IntentKeys
import com.example.onlinequiz.database.QuizModel
import com.example.onlinequiz.database.QuizRepository
import com.example.onlinequiz.databinding.QuizlistActBinding
import com.example.onlinequiz.home.adapter.QuizAdapter
import kotlinx.android.synthetic.main.custom_actionbar.view.*

class QuizListAct : BaseActivity(), View.OnClickListener {

    //Variable declarations
    lateinit var binding : QuizlistActBinding
    lateinit var sharedPreference: SharedPreference
    lateinit var quizAdapter : QuizAdapter

    //Declare Database Repository
    private lateinit var quizRepository: QuizRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.quizlist_act)
        hideActionBar()

        //Initializing
        quizRepository = QuizRepository(applicationContext)
        sharedPreference = SharedPreference(applicationContext)
        binding.actionbarLyt.actionNameTxt.setText("Welcome "+sharedPreference.getValueString(IntentKeys.INSTANCE.NAME))

        //Init UI based on auth
        if(intent.getStringExtra(IntentKeys.INSTANCE.AUTH_TYPE).equals(IntentKeys.INSTANCE.TEACHER,true)){
            binding.quizCreateBtn.visibility = View.VISIBLE
            binding.startBtn.visibility = View.GONE
        }else{
            binding.quizCreateBtn.visibility = View.GONE
            binding.startBtn.visibility = View.VISIBLE
        }

        //Show/Hide logout event based on login
        if(sharedPreference.loginCheck()){
            binding.actionbarLyt.logoutTxt.visibility=View.VISIBLE
        }else{
            binding.actionbarLyt.logoutTxt.visibility=View.VISIBLE
        }

        //Crete click event
        binding.startBtn.setOnClickListener(this)
        binding.quizCreateBtn.setOnClickListener(this)
        binding.actionbarLyt.logoutTxt.setOnClickListener(this)
        binding.actionbarLyt.actionBackImg.setOnClickListener(this)

    }

    //Click events
    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.startBtn->{startActivity(Intent(this,StartQuizAct::class.java))}
            R.id.quizCreateBtn->{startActivity(Intent(this,CreateQuizAct::class.java))}
            R.id.logoutTxt->{logout()}
            R.id.actionBackImg->{finish()}
        }
    }

    //Get database values
    private fun getQuizList() {
        quizRepository!!.getTasks()
            ?.observe(this, object : Observer<List<QuizModel?>?> {
                override fun onChanged(t: List<QuizModel?>?) {
                    if (t!!.size > 0) {
                        //Set recyclerview
                        binding.quizListView.setLayoutManager(StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL))
                        quizAdapter = QuizAdapter(t as ArrayList<QuizModel>, applicationContext)
                        binding.quizListView.adapter = quizAdapter
                    } else {
                        feedDummyRecord()
                    }
                }
            })
    }

    //Feed Dummy Record to database
    private fun feedDummyRecord() {
        quizRepository.insertTask("GK",IntentKeys.INSTANCE.getTasks())
    }

    override fun onResume() {
        super.onResume()
        //Get QuizList from Database
        getQuizList()
    }

    //Logout function
    private fun logout() {
        sharedPreference.save(IntentKeys.INSTANCE.IS_LOGIN,false)
        finish()
        toast("LogoutSuccess")
    }

}