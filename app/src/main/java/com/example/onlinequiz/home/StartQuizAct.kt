package com.example.onlinequiz.home

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.onlinequiz.R
import com.example.onlinequiz.base.BaseActivity
import com.example.onlinequiz.base.SharedPreference
import com.example.onlinequiz.common.IntentKeys
import com.example.onlinequiz.databinding.StartquizActBinding
import com.kork.customer.common.timmer.SharedViewModel
import org.json.JSONArray

class StartQuizAct : BaseActivity(), CompoundButton.OnCheckedChangeListener {

    //Variable declarations
    lateinit var binding : StartquizActBinding
    lateinit var sharedPreference: SharedPreference

    var questionNumber : Int = 0
    var correctAnswerCount : Int = 0
    var incorrectAnswerCount : Int = 0
    lateinit var questionJSONArray : JSONArray

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.startquiz_act)
        hideActionBar()

        //Initializing
        sharedPreference = SharedPreference(this)

        //Set Action Title
        binding.actionNameTxt.setText(sharedPreference.getValueString(IntentKeys.INSTANCE.QUIZ_NAME))

        //Create and start timmer
        observe()
        val sharedViewModel = ViewModelProviders.of(this).get(SharedViewModel::class.java)
        sharedViewModel.startTimer()

        //Get and show questions
        questionJSONArray = JSONArray(sharedPreference.getValueString(IntentKeys.INSTANCE.QUIZ_QUESTION))
        updateQuestionUI(questionNumber)

        //Option click event
        binding.optionATxt.setOnCheckedChangeListener(this)
        binding.optionBTxt.setOnCheckedChangeListener(this)
        binding.optionCTxt.setOnCheckedChangeListener(this)
        binding.optionDTxt.setOnCheckedChangeListener(this)

        //Back event
        binding.actionBackImg.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
    }

    //Timmer method
    fun observe() {
        // Creating an observer
        val timeObserver = Observer<String> { formattedTime ->
            if (formattedTime != null) {
                binding.countdownTxt.text=formattedTime
            }
        }
        val sharedViewModel = ViewModelProviders.of(this).get(SharedViewModel::class.java)
        sharedViewModel.formattedTime.observe(this as LifecycleOwner, timeObserver)
    }

    //Question Handler
    fun questionsHandler(answerText : String){

        if(answerText.toString().trim().equals(questionJSONArray.getJSONObject(questionNumber).getString(IntentKeys.INSTANCE.ANSWER),true)){
            onSNACK_CORRECT()
            correctAnswerCount = correctAnswerCount+1
        }else{
            onSNACK_WRONG()
            incorrectAnswerCount = incorrectAnswerCount+1
        }
        Handler().postDelayed({
            Log.e("############",""+questionNumber)
            if(questionNumber <= 8){
                toast("Next Question")
                questionNumber = questionNumber+1
                updateQuestionUI(questionNumber)
            }else{
                sharedPreference.save(IntentKeys.INSTANCE.TIME_TAKEN,binding.countdownTxt.text.toString().trim())
                sharedPreference.save(IntentKeys.INSTANCE.CORRECT_COUNT,correctAnswerCount.toString().trim())
                sharedPreference.save(IntentKeys.INSTANCE.INCORRECT_COUNT,incorrectAnswerCount.toString().trim())
                val sharedViewModel = ViewModelProviders.of(this).get(SharedViewModel::class.java)
                sharedViewModel.stopTimer()
                startActivity(Intent(this,ResultAct::class.java))
                finish()
            }
        }, 1000)
    }

    //Get and show questions
    private fun updateQuestionUI(position: Int) {
        binding.questionTxt.setText((position+1).toString()+". "+questionJSONArray.getJSONObject(position).getString(IntentKeys.INSTANCE.QUESTION))
        binding.optionATxt.setText(questionJSONArray.getJSONObject(position).getString(IntentKeys.INSTANCE.OPTION_A))
        binding.optionBTxt.setText(questionJSONArray.getJSONObject(position).getString(IntentKeys.INSTANCE.OPTION_B))
        binding.optionCTxt.setText(questionJSONArray.getJSONObject(position).getString(IntentKeys.INSTANCE.OPTION_C))
        binding.optionDTxt.setText(questionJSONArray.getJSONObject(position).getString(IntentKeys.INSTANCE.OPTION_D))
        binding.countBtn.setText((position+1).toString()+"/"+"10")

        binding.radioGroup.clearCheck()
    }


    //onBackPressed event
    override fun onBackPressed() {
        onSNACK("Please complete the Quiz")
    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        when(p0!!.id){
            R.id.optionATxt->{
                if(p1){questionsHandler(binding.optionATxt.text.toString().toString())}
            }
            R.id.optionBTxt->{
                if(p1){questionsHandler(binding.optionBTxt.text.toString().toString())}
            }
            R.id.optionCTxt->{
                if(p1){questionsHandler(binding.optionCTxt.text.toString().toString())}
            }
            R.id.optionDTxt->{
                if(p1){questionsHandler(binding.optionDTxt.text.toString().toString())}
            }
        }
    }
}
