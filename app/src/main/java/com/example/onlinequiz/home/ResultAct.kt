package com.example.onlinequiz.home

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.onlinequiz.R
import com.example.onlinequiz.base.BaseActivity
import com.example.onlinequiz.base.SharedPreference
import com.example.onlinequiz.common.IntentKeys
import com.example.onlinequiz.databinding.ResultActBinding

class ResultAct : BaseActivity() {

    //Variable declarations
    lateinit var binding : ResultActBinding
    lateinit var sharedPreference: SharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.result_act)
        showBackArrow()
        setActionBarTitle(resources.getString(R.string.Result))

        //Initializing
        sharedPreference = SharedPreference(applicationContext)

        //Set Ui
        binding.thankYouTxt.setText(resources.getString(R.string.Thanks)+" "+sharedPreference.getValueString(IntentKeys.INSTANCE.NAME))
        binding.correctAnswerTxt.setText(sharedPreference.getValueString(IntentKeys.INSTANCE.CORRECT_COUNT))
        binding.incorrectAnswerTxt.setText(sharedPreference.getValueString(IntentKeys.INSTANCE.INCORRECT_COUNT))
        binding.timeTakenTxt.setText(sharedPreference.getValueString(IntentKeys.INSTANCE.TIME_TAKEN))
        binding.totalMarkTxt.setText(sharedPreference.getValueString(IntentKeys.INSTANCE.CORRECT_COUNT)+" mark")

        binding.homeBtn.setOnClickListener(View.OnClickListener {
            finish()
        })
    }
}