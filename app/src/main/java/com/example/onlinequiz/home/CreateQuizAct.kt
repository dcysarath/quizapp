package com.example.onlinequiz.home

import android.os.Bundle
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.databinding.DataBindingUtil
import com.example.onlinequiz.R
import com.example.onlinequiz.base.BaseActivity
import com.example.onlinequiz.database.QuestionModel
import com.example.onlinequiz.database.QuizRepository
import com.example.onlinequiz.databinding.CreatequizAxtBinding
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class CreateQuizAct : BaseActivity() {

    //Variable declarations
    lateinit var binding : CreatequizAxtBinding
    val questionModels = ArrayList<QuestionModel>()
    var questionNumber : Int = 1

    //Declare Database Repository
    private lateinit var quizRepository: QuizRepository


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.createquiz_axt)
        showBackArrow()
        setActionBarTitle(resources.getString(R.string.CreateQuiz))

        //Initializing
        quizRepository = QuizRepository(applicationContext)
        binding.questionTxt.setText("Question NO - "+questionNumber.toString())

        //Insert event
        binding.confirmBtn.setOnClickListener { view ->

            val name : String = binding.quizNameExt.text.toString()
            val question : String= binding.quizQuestionExt.text.toString()
            val optionA : String= binding.optionAExt.text.toString()
            val optionB : String= binding.optionBExt.text.toString()
            val optionC : String= binding.optionCExt.text.toString()
            val optionD : String= binding.optionDExt.text.toString()
            var answer : String= ""

            if(name.isEmpty()){
                binding.quizNameExt.requestFocus()
                binding.quizNameExt.setError(resources.getString(R.string.PleaseEnterAName))
            }else if(question.isEmpty()){
                binding.quizQuestionExt.requestFocus()
                binding.quizQuestionExt.setError(resources.getString(R.string.EnterQuestion))
            }else if(optionA.isEmpty()){
                binding.optionAExt.requestFocus()
                binding.optionAExt.setError(resources.getString(R.string.EnterOptionA))
            }else if(optionB.isEmpty()){
                binding.optionBExt.requestFocus()
                binding.optionBExt.setError(resources.getString(R.string.EnterOptionB))
            }else if(optionC.isEmpty()){
                binding.optionCExt.requestFocus()
                binding.optionCExt.setError(resources.getString(R.string.EnterOptionC))
            }else if(optionD.isEmpty()){
                binding.optionDExt.requestFocus()
                binding.optionDExt.setError(resources.getString(R.string.EnterOptionD))
            }else{

                binding.radioGroup.setOnCheckedChangeListener(
                    RadioGroup.OnCheckedChangeListener { group, checkedId ->
                        val radio: RadioButton = findViewById(checkedId)
                        if(radio.text.equals("A")){
                            answer = optionA
                        }else if(radio.text.equals("B")){
                            answer = optionB
                        }else if(radio.text.equals("C")){
                            answer = optionC
                        }else{
                            answer = optionD
                        }
                    })

                //Insert to arrayList
                val questionModel = QuestionModel()
                questionModel.question = question;
                questionModel.optionA = optionA;
                questionModel.optionB = optionB;
                questionModel.optionC = optionC;
                questionModel.optionD = optionD;
                questionModel.answer = answer;
                questionModels.add(questionModel)

                if(questionNumber == 10){
                    insertToDataBase()
                }else{
                    //Update UI
                    updateUI()
                }

            }

        }


    }

    //Update UI after insert local arrayList
    private fun updateUI() {
        questionNumber = questionNumber+1
        binding.questionTxt.setText("Question NO - "+questionNumber.toString())
        binding.quizQuestionExt.setText("")
        binding.optionAExt.setText("")
        binding.optionBExt.setText("")
        binding.optionCExt.setText("")
        binding.optionDExt.setText("")
    }

    //Insert values to database
    private fun insertToDataBase() {

        val jsonArray = JSONArray()
        try {
            for(data in questionModels){
                val jsonObject = JSONObject()
                jsonObject.put("question", data.question)
                jsonObject.put("optionA", data.optionA)
                jsonObject.put("optionB", data.optionB)
                jsonObject.put("optionC", data.optionC)
                jsonObject.put("optionD", data.optionD)
                jsonObject.put("answer", data.answer)
                jsonArray.put(jsonObject)
            }
        }catch (e: JSONException){e.printStackTrace()}

        quizRepository.insertTask(binding.quizNameExt.text.toString().trim(), jsonArray.toString())
        onSNACK("Quiz Added Successfully")
        finish()
    }
}