package com.example.onlinequiz.home.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.onlinequiz.R
import com.example.onlinequiz.base.SharedPreference
import com.example.onlinequiz.common.IntentKeys
import com.example.onlinequiz.database.QuizModel
import kotlinx.android.synthetic.main.custom_quizlist.view.*

class QuizAdapter(val items: ArrayList<QuizModel>, val context: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<ViewHolder>() {

    var sharedPreference: SharedPreference = SharedPreference(context)
    var seletedPosition : Int = 0

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_quizlist, parent, false))
    }

    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(seletedPosition == position){
            sharedPreference.save(IntentKeys.INSTANCE.QUIZ_NAME,items.get(position).title)
            sharedPreference.save(IntentKeys.INSTANCE.QUIZ_QUESTION,items.get(position).questionModel)
            holder?.cardLayoutBg.setBackgroundDrawable(context.resources.getDrawable(R.drawable.black_border))
        }else{
            holder?.cardLayoutBg.setBackgroundDrawable(context.resources.getDrawable(R.drawable.gray_border))
        }

        holder?.quizNametxt?.text = items.get(position).title

        holder?.cardLayoutBg.setOnClickListener {
            seletedPosition = position
            notifyDataSetChanged()
        }
    }
}

class ViewHolder (view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val quizNametxt = view.quizNametxt
    val cardLayoutBg = view.cardLayoutBg
}