package com.kork.customer.common.timmer

import java.util.Date

data class TimerStateModel(var isRunning: Boolean, var startedAt: Date?)