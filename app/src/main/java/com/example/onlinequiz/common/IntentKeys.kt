package com.example.onlinequiz.common

import com.example.onlinequiz.database.QuestionModel
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class IntentKeys {

    //Create Singleton objects
    private object Holder {
        val INSTANCE = IntentKeys()
    }
    companion object {
        val INSTANCE: IntentKeys by lazy {
            Holder.INSTANCE }
    }

    //Common Parameter
    val AUTH_TYPE : String      = "AUTH_TYPE"
    val TEACHER : String        = "TEACHER"
    val STUDENT : String        = "STUDENT"
    val IS_LOGIN : String       = "IS_LOGIN"
    val NUMBER : String         = "USER_NUMBER"
    val NAME : String           = "USER_NAME"
    val QUIZ_NAME : String      = "QUIZ_NAME"
    val QUIZ_QUESTION : String  = "QUIZ_QUESTION"

    /*Question Model Obj*/
    val QUESTION : String  = "question"
    val OPTION_A : String  = "optionA"
    val OPTION_B : String  = "optionB"
    val OPTION_C: String   = "optionC"
    val OPTION_D: String   = "optionD"
    val ANSWER : String    = "answer"

    val TIME_TAKEN : String       = "TIME_TAKEN"
    val CORRECT_COUNT : String    = "CORRECT_COUNT"
    val INCORRECT_COUNT : String  = "INCORRECT_COUNT"

    fun getTasks(): String {

        val questionModels = ArrayList<QuestionModel>()

        //QUESTION NO : 1
        val questionModel_1 = QuestionModel()
        questionModel_1.question = "Which best selling toy of 1983 caused hysteria, resulting in riots breaking out in stores?";
        questionModel_1.optionA = "Transformers";
        questionModel_1.optionB = "Care Bears";
        questionModel_1.optionC = "Rubik&rsquos Cube";
        questionModel_1.optionD = "Cabbage Patch Kids";
        questionModel_1.answer = "Cabbage Patch Kids";
        questionModels.add(questionModel_1)

        //QUESTION NO : 2
        val questionModel_2 = QuestionModel()
        questionModel_2.question = "Which of these colours is NOT featured in the logo for Google?";
        questionModel_2.optionA = "Yellow";
        questionModel_2.optionB = "Pink";
        questionModel_2.optionC = "Blue";
        questionModel_2.optionD = "Green";
        questionModel_2.answer = "Pink";
        questionModels.add(questionModel_2)

        //QUESTION NO : 3
        val questionModel_3 = QuestionModel()
        questionModel_3.question = "Who is depicted on the US hundred dollar bill?";
        questionModel_3.optionA = "Benjamin Franklin";
        questionModel_3.optionB = "George Washington";
        questionModel_3.optionC = "Abraham Lincoln";
        questionModel_3.optionD = "Thomas Jefferson";
        questionModel_3.answer = "Benjamin Franklin";
        questionModels.add(questionModel_3)

        //QUESTION NO : 4
        val questionModel_4 = QuestionModel()
        questionModel_4.question = "The likeness of which president is featured on the rare $2 bill of USA currency?";
        questionModel_4.optionA = "Thomas Jefferson";
        questionModel_4.optionB = "Martin Van Buren";
        questionModel_4.optionC = "Ulysses Grant";
        questionModel_4.optionD = "John Quincy Adams";
        questionModel_4.answer = "Thomas Jefferson";
        questionModels.add(questionModel_4)

        //QUESTION NO : 5
        val questionModel_5 = QuestionModel()
        questionModel_5.question = "What company developed the vocaloid Hatsune Miku?";
        questionModel_5.optionA = "Sega";
        questionModel_5.optionB = "Crypton Future Media";
        questionModel_5.optionC = "Sony";
        questionModel_5.optionD = "Yamaha Corporation";
        questionModel_5.answer = "Crypton Future Media";
        questionModels.add(questionModel_5)

        //QUESTION NO : 6
        val questionModel_6 = QuestionModel()
        questionModel_6.question = "What is Tasmania?";
        questionModel_6.optionA = "A Psychological Disorder";
        questionModel_6.optionB = "A flavor of Ben and Jerry&#039;s ice-cream";
        questionModel_6.optionC = "An Australian State";
        questionModel_6.optionD = "The Name of a Warner Brothers Cartoon Character";
        questionModel_6.answer = "An Australian State";
        questionModels.add(questionModel_6)

        //QUESTION NO : 7
        val questionModel_7 = QuestionModel()
        questionModel_7.question = "What is the name of Poland in Polish?";
        questionModel_7.optionA = "Pupcia";
        questionModel_7.optionB = "Polszka";
        questionModel_7.optionC = "Polska";
        questionModel_7.optionD = "P&oacute;land";
        questionModel_7.answer = "Polska";
        questionModels.add(questionModel_7)

        //QUESTION NO : 8
        val questionModel_8 = QuestionModel()
        questionModel_8.question = "What is the French word for &quot;hat&quot;?";
        questionModel_8.optionA = "Bonnet";
        questionModel_8.optionB = "Chapeau";
        questionModel_8.optionC = "Casque";
        questionModel_8.optionD = "P&oacute;land";
        questionModel_8.answer = "Chapeau";
        questionModels.add(questionModel_8)

        //QUESTION NO : 9
        val questionModel_9 = QuestionModel()
        questionModel_9.question = "After how many years would you celebrate your crystal anniversary?";
        questionModel_9.optionA = "32";
        questionModel_9.optionB = "10";
        questionModel_9.optionC = "20";
        questionModel_9.optionD = "15";
        questionModel_9.answer = "15";
        questionModels.add(questionModel_9)

        //QUESTION NO : 10
        val questionModel_10 = QuestionModel()
        questionModel_10.question = "Which of the following buildings is example of a structure primarily built in the Art Deco architectural style?";
        questionModel_10.optionA = "Niagara Mohawk Building";
        questionModel_10.optionB = "One Detroit Center";
        questionModel_10.optionC = "Westendstrasse 1";
        questionModel_10.optionD = "Taipei 101";
        questionModel_10.answer = "Niagara Mohawk Building";
        questionModels.add(questionModel_10)

        val jsonArray = JSONArray()
        try {
            for(data in questionModels){
                val jsonObject = JSONObject()
                jsonObject.put("question", data.question)
                jsonObject.put("optionA", data.optionA)
                jsonObject.put("optionB", data.optionB)
                jsonObject.put("optionC", data.optionC)
                jsonObject.put("optionD", data.optionD)
                jsonObject.put("answer", data.answer)
                jsonArray.put(jsonObject)
            }
        }catch (e:JSONException){e.printStackTrace()}

        return jsonArray.toString();
    }
}