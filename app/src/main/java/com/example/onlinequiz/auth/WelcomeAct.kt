package com.example.onlinequiz.auth

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.onlinequiz.R
import com.example.onlinequiz.base.BaseActivity
import com.example.onlinequiz.common.IntentKeys
import com.example.onlinequiz.databinding.WelcomeActBinding

class WelcomeAct : BaseActivity(), View.OnClickListener {

    //Variable declarations
    lateinit var binding : WelcomeActBinding
    val TEACHER : String = "TEACHER"
    val STUDENT : String = "STUDENT"
    var AUTH_TYPE : String = TEACHER

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.welcome_act)
        setActionBarTitle(resources.getString(R.string.app_name))
        showBackArrow()

        //Create click events
        binding.teacherLyt.setOnClickListener(this)
        binding.studentLyt.setOnClickListener(this)
        binding.continueBtn.setOnClickListener(this)
    }

    override fun onClick(view: View?) {

        when(view!!.id){

            R.id.teacherLyt->{
                AUTH_TYPE = TEACHER
                binding.teacherLyt.setBackgroundResource(R.drawable.black_border)
                binding.studentLyt.setBackgroundResource(R.drawable.gray_border)
            }

            R.id.studentLyt->{
                AUTH_TYPE = STUDENT
                binding.teacherLyt.setBackgroundResource(R.drawable.gray_border)
                binding.studentLyt.setBackgroundResource(R.drawable.black_border)
            }

            R.id.continueBtn->{
                startActivity(Intent(this,LoginAct::class.java)
                    .putExtra(IntentKeys.INSTANCE.AUTH_TYPE,AUTH_TYPE))
            }
        }
    }
}