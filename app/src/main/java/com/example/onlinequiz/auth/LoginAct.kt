package com.example.onlinequiz.auth

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.onlinequiz.R
import com.example.onlinequiz.base.BaseActivity
import com.example.onlinequiz.base.SharedPreference
import com.example.onlinequiz.common.IntentKeys
import com.example.onlinequiz.databinding.LoginActBinding
import com.example.onlinequiz.home.QuizListAct
import com.google.firebase.auth.FirebaseAuth

class LoginAct : BaseActivity(), View.OnClickListener {

    //Variable declarations
    lateinit var binding : LoginActBinding
    lateinit var sharedPreference: SharedPreference

    //Firebase auth object
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.login_act)
        showBackArrow()
        setActionBarTitle(resources.getString(R.string.Login))

        //Initialize Objects
        mAuth = FirebaseAuth.getInstance();
        sharedPreference = SharedPreference(this)

        //Create click events
        binding.singnUpBtn.setOnClickListener(this)
        binding.guestBtn.setOnClickListener(this)
    }

    //Click Events
    override fun onClick(view: View?) {

        when(view!!.id){

            R.id.singnUpBtn->{

                val name : String = binding.nameExt.text.toString()
                val number : String= binding.numberEditText.text.toString()

                if(name.isEmpty()){
                    binding.nameExt.requestFocus()
                    binding.nameExt.setError(resources.getString(R.string.PleaseEnterAName))
                }else if(number.isEmpty()){
                    binding.numberEditText.requestFocus()
                    binding.numberEditText.setError(resources.getString(R.string.PleaseEnterANumber))
                }else if(!(number.length == 10)){
                    binding.numberEditText.requestFocus()
                    binding.numberEditText.setError(resources.getString(R.string.PleaseEnterAValidNumber))
                }else if(!checkInternet()){
                    onSNACK(resources.getString(R.string.InterNetCheck))
                }else{
                    startActivity(
                        Intent(this, OtpScreenAct::class.java)
                        .putExtra(IntentKeys.INSTANCE.NUMBER,number)
                            .putExtra(IntentKeys.INSTANCE.NAME,name)
                            .putExtra(IntentKeys.INSTANCE.AUTH_TYPE,intent.getStringExtra(IntentKeys.INSTANCE.AUTH_TYPE)))

                }
            }

            R.id.guestBtn->{

                val name : String = binding.nameExt.text.toString()
                if(name.isEmpty()){
                    binding.nameExt.requestFocus()
                    binding.nameExt.setError(resources.getString(R.string.PleaseEnterAName))
                }else{
                    sharedPreference.save(IntentKeys.INSTANCE.NAME,name)
                    startActivity(
                        Intent(this, QuizListAct::class.java)
                            .putExtra(IntentKeys.INSTANCE.AUTH_TYPE,intent.getStringExtra(IntentKeys.INSTANCE.AUTH_TYPE)))
                }

            }
        }
    }
}