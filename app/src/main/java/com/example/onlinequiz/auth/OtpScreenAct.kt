package com.example.onlinequiz.auth

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.onlinequiz.R
import com.example.onlinequiz.base.BaseActivity
import com.example.onlinequiz.base.SharedPreference
import com.example.onlinequiz.common.IntentKeys
import com.example.onlinequiz.databinding.OtpScreenBinding
import com.example.onlinequiz.home.QuizListAct
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseException
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit


class OtpScreenAct : BaseActivity() {

    //Variable declarations
    lateinit var binding: OtpScreenBinding
    lateinit var sharedPreference: SharedPreference
    var mVerificationId: String = ""

    //firebase auth object
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.otp_screen)
        showBackArrow()
        setActionBarTitle(resources.getString(R.string.SMSVerify))

        //Initialize
        mAuth = FirebaseAuth.getInstance();
        sharedPreference = SharedPreference(this)

        binding.numberTextView.setText(resources.getString(R.string.ShowNumber)+intent.getStringExtra(IntentKeys.INSTANCE.NUMBER))

        sendVerificationCode()

        binding.verifyButton.setOnClickListener {

            val code : String = binding.otpEditText.text.toString()
            if(code.isEmpty()){
                binding.otpEditText.requestFocus()
                binding.otpEditText.setError(resources.getString(R.string.PleaseEnterAOtp))
            }else if(code.equals(mVerificationId)){
                binding.otpEditText.requestFocus()
                binding.otpEditText.setError(resources.getString(R.string.PleaseEnterAValidOtp))
            }else if(!checkInternet()){
                onSNACK(resources.getString(R.string.InterNetCheck))
            }else{
                binding.loadingProgressbar.visibility = View.VISIBLE
                binding.verifyButton.setBackgroundResource(R.drawable.disable_button)
                binding.verifyButton.isEnabled = false
                verifyVerificationCode(code)
            }
        }

        binding.resentButton.setOnClickListener {
            sendVerificationCode();
        }
    }

    //Send OTP
    private fun sendVerificationCode() {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            "+91" + intent.getStringExtra(IntentKeys.INSTANCE.NUMBER),        // Phone number to verify
            60,                 // Timeout duration
            TimeUnit.SECONDS,   // Unit of timeout
            this,               // Activity (for callback binding)
            mCallbacks
        );        // OnVerificationStateChangedCallbacks
    }

    //the callback to detect the verification status
    private val mCallbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks =
        object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {

                //Getting the code sent by SMS
                val code = phoneAuthCredential.smsCode
                binding.otpEditText.setText(code)

            }

            override fun onVerificationFailed(e: FirebaseException) {
                onSNACK(e.message!!)
            }

            override fun onCodeSent(
                s: String,
                forceResendingToken: PhoneAuthProvider.ForceResendingToken
            ) {
                super.onCodeSent(s, forceResendingToken)
                mVerificationId = s
            }
        }

    private fun verifyVerificationCode(code: String) {
        val credential = PhoneAuthProvider.getCredential(mVerificationId, code)
        signInWithPhoneAuthCredential(credential)
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(
                this,
                OnCompleteListener<AuthResult?> { task ->
                    if (task.isSuccessful) {
                        onSNACK(resources.getString(R.string.Success))
                        sharedPreference.save(IntentKeys.INSTANCE.IS_LOGIN,true);
                        sharedPreference.save(IntentKeys.INSTANCE.NAME,
                            intent.getStringExtra(IntentKeys.INSTANCE.NAME)!!
                        )
                        sharedPreference.save(IntentKeys.INSTANCE.AUTH_TYPE,intent.getStringExtra(IntentKeys.INSTANCE.AUTH_TYPE)!!)
                        startActivity(Intent(this, QuizListAct::class.java)
                            .putExtra(IntentKeys.INSTANCE.AUTH_TYPE,intent.getStringExtra(IntentKeys.INSTANCE.AUTH_TYPE))
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
                        finish()
                    } else {
                        var message = resources.getString(R.string.SomethingWrong)
                        onSNACK(message);
                        binding.loadingProgressbar.visibility = View.INVISIBLE
                        binding.verifyButton.setBackgroundResource(R.drawable.button_bg)
                        binding.verifyButton.isEnabled = true
                    }
                })
    }
}